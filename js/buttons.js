function addInputField() {
  document.getElementById("input1").style.display = "block";
}

function addToList() {
  const clothesList = document.getElementById("clothes-list");
  const listElement = document.createElement("li");
  const checkbox = document.createElement("input");
  checkbox.type = "checkbox"; 
  const link = document.createElement("a");
  link.textContent = document.getElementById("input1").value;
  link.href="https://www.newurl.com";
  listElement.appendChild(checkbox);
  listElement.appendChild(link);
  clothesList.appendChild(listElement);
}

function deleteListItem() {
    const checkboxes = document.querySelectorAll("input[type=checkbox]");
    checkboxes.forEach(function(checkbox) {
      if (checkbox.checked) {
        checkbox.parentNode.remove();
      }
    });
}