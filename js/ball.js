const ballContainer = document.querySelector(".ball-container");
const firstParagraph = document.querySelector(".first-paragraph");
const secondParagraph = document.querySelector(".second-paragraph");
const thirdParagraph = document.querySelector(".third-paragraph");
let ball = null;

function jumpToParagraph() {
  if (!ball) {
    ball = document.createElement("div");
    ballContainer.appendChild(ball);
    ball.className = "ball";
    ball.addEventListener("animationend", removeBall);
  }
}

function removeBall() {
  if (ball) {
    ball.remove();
    ball = null;
  }
}

firstParagraph.addEventListener("click", jumpToParagraph);
secondParagraph.addEventListener("click", jumpToParagraph);
thirdParagraph.addEventListener("click", jumpToParagraph);

